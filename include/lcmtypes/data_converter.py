#!/usr/bin/python

import lcm

from lcmtypes_py import *

file_imu = open("/home/student/team11/quadrotor/include/lcmtypes/imu.csv","w")
file_local_state = open("/home/student/team11/quadrotor/include/lcmtypes/local_state.csv", "w")
file_pid_outputs = open("/home/student/team11/quadrotor/include/lcmtypes/pid_outputs.csv", "w")
file_pilot_state = open("/home/student/team11/quadrotor/include/lcmtypes/pilot_state.csv", "w")
file_set_point = open("/home/student/team11/quadrotor/include/lcmtypes/set_point.csv", "w")
file_wypt_data = open("/home/student/team11/quadrotor/include/lcmtypes/wypt_data.csv","w")

def handler_imu(channel, data):
	msg = imumsg_t.decode(data)
	file_imu.write("%d,%f,%f,%f,%f,%f,%f\n"%(msg.utime, msg.gyro_x, msg.gyro_y, msg.gyro_z, msg.accel_x, msg.accel_y, msg.accel_z))
	file_imu.flush()

def handler_localstate(channel, data):
	msg = localstate_t.decode(data)
	file_local_state.write("%d,%f,%f,%f,%f,%f,%f,%f,%f\n"%(msg.utime, msg.x, msg.y, msg.alt, msg.yaw, msg.x_dot, msg.y_dot, msg.alt_dot, msg.yaw_dot))
	file_local_state.flush()

def handler_pid_outputs(channel, data):
	msg = pidstate_t.decode(data)
	file_pid_outputs.write("%d,%f,%f,%f,%f\n"%(msg.utime, msg.thrust_out, msg.roll_out, msg.pitch_out, msg.yaw_out))
	file_pid_outputs.flush()

def handler_pilot_state(channel, data):
	msg = pilotstate_t.decode(data)
	file_pilot_state.write("%d,%f,%f,%f,%f,%d\n"%(msg.utime, msg.thrust_out, msg.roll_out, msg.pitch_out, msg.yaw_out, msg.auto_on))
	file_pilot_state.flush()

def handler_set_point(channel, data):
	msg = setpnt_t.decode(data)
	file_set_point.write("%d,%f,%f,%f,%f\n"%(msg.utime, msg.x_set, msg.y_set, msg.alt_set, msg.yaw_set))
	file_set_point.flush()

def handler_wypt_data(channel, data):
	msg = wyptdata_t.decode(data)
	file_wypt_data.write("%d,%d,%f,%f,%f,%f,%d\n"%(msg.utime, msg.wypt_num, msg.norm, msg.yaw_err, msg.y_err, msg.landing_tol, msg.status))
	file_wypt_data.flush()

lc = lcm.LCM()
lcm_imu = lc.subscribe("IMU", handler_imu)
lcm_local_state = lc.subscribe("LOCALSTATE", handler_localstate)
lcm_pid_outputs = lc.subscribe("PID_OUTPUTS", handler_pid_outputs)
lcm_pilot_state = lc.subscribe("PILOT_STATE", handler_pilot_state)
lcm_set_point   = lc.subscribe("SET_POINT", handler_set_point)
lcm_set_point   = lc.subscribe("WYPT_DATA", handler_wypt_data)
while 1:
	lc.handle()