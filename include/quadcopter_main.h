#ifndef QUADCOPTER_GLOBALS
#define QUADCOPTER_GLOBALS

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

//Use this in your code:  #define EXTERN extern

#include <lcm/lcm.h>
#include "../include/lcmtypes/lcmtypes_c/channels_t.h"
#include "../include/lcmtypes/lcmtypes_c/imumsg_t.h"
#include "../include/lcmtypes/lcmtypes_c/setpnt_t.h"
#include "../include/lcmtypes/lcmtypes_c/pidstate_t.h"
#include "../include/lcmtypes/lcmtypes_c/localstate_t.h"
#include "../include/lcmtypes/lcmtypes_c/pilotstate_t.h"
#include "../include/lcmtypes/lcmtypes_c/wyptdata_t.h"
#include "../include/lcmtypes/lcmtypes_c/machinestate_t.h"
#include "../include/quadcopter_struct.h"
#include "../include/bbblib/bbb.h"

// Primary threads
#include "../include/run_imu.h"
#include "../include/run_motion_capture.h"
#include "../include/util.h"

//#define MCAP_TESTING
#define WYPT_FOLLOW 1     //<-- define for Wypt following test sequence
//#define TEST_SEQUENCE 1   //<-- define for Wypt following test sequence
#define GRIPPER_UNPERCH_SEQUENCE
#define MANUAL_PERCH 1

/* Global Variables with mutexes for sharing */
//EXTERN lcm_t* lcm  = lcm_create(NULL);
EXTERN imu_t imudata;
EXTERN struct motion_capture_obs mcap_obs[2];
EXTERN state_t *state;
EXTERN DynamBus bus;

/* Mutexes */
EXTERN pthread_mutex_t imu_mutex; 
EXTERN pthread_mutex_t mcap_mutex;  
EXTERN pthread_mutex_t state_mutex;
EXTERN pthread_mutex_t dynamixel_mutex;

/* Global variables that are not used in multiple threads (no mutex use) */
EXTERN FILE *mcap_txt, *block_txt, *imu_txt, *log_txt; // Output data files

EXTERN int imu_mode, mcap_mode, gripper_desired_state, state_machine_status; //Gripper 0 - OPEN, 1 - CLOSED
EXTERN struct imu_data *imu; 
EXTERN DynamSetup dynam; 

// Top-level thread function declarations
void *lcm_thread_loop(void *);
void *processing_loop(void *);
void *run_imu(void *);
void *run_motion_capture(void *);

/* Function declarations needed for multiple files */
void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
		      const channels_t *msg, void *userdata);
void auto_control(float *pose, float *set_points, int16_t *channels_ptr);

//////////////////////////////////////////
///  Geofence stuff for testing

#define NUM_SAMPLES_MED_ALT 10
#define NUM_SAMPLES_AVG_ALT 20

EXTERN float KP_thrust, KI_thrust, KD_thrust;
EXTERN float KP_pitch, KI_pitch, KD_pitch;
EXTERN float KP_roll, KI_roll, KD_roll;
EXTERN float KP_yaw, KI_yaw, KD_yaw;

EXTERN double diff_z[NUM_SAMPLES_MED_ALT], diff_z_med[NUM_SAMPLES_AVG_ALT];
EXTERN double start[2];
EXTERN double ang_buf;
EXTERN double fence_penalty_length;

// max desired velocity for fence corrections
EXTERN float max_vel, max_step;

EXTERN double alt_low_fence, alt_high_fence;
EXTERN double x_pos_fence, x_neg_fence, y_pos_fence, y_neg_fence;
EXTERN double alt_prev;

//////////////////////////////////////////////////////////////////////////////
// Geofence function
//int update_set_points(double* pose, float* set_points, int first_time);
int update_set_points(double* pose, float *set_points, int first_time, float* bar_center, float* start_pos, float move_alt,  lcm_t* lcm);



// State Machine Status

#define SM_PILOT_FLYING 1
#define SM_LANDING 2
#define SM_PERCH 3
#define SM_PERCHED 4
#define SM_UNPERCH 5
#define SM_UNPERCHED 6
#define SM_TAKEOFF 7
#define SM_HOLD 8


// Gripper Constants
#define GRIP_STATUS_CLOSED 0
#define GRIP_STATUS_OPEN 1
#define CLOSE_GRIPPER 0
#define OPEN_GRIPPER 1




#ifdef __cplusplus
}
#endif

#endif
