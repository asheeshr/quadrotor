#ifndef ROB550_STRUCT
#define ROB550_STRUCT

// Shared data structures for ROB 550 Quadrotor project (not LCM)

typedef struct imu imu_t;
struct imu  // Only includes gyro and accel data for now
{
  int64_t utime;
  double gyro_x, gyro_y, gyro_z;
  double accel_x, accel_y, accel_z;
};

/*
 * state:
 * struct holds values related to current state estimate and 
 * autonomous control (fence) activity
*/
typedef struct state state_t;
struct state{
  double time;

  // aircraft position (x,y,alt,yaw,xdot,ydot,altdot,yawdot)
  double pose[8];

  // Flag indicating whether the autonomous controller is activ or not
  // (1 = on; 0 = pass through pilot commands)
  int fence_on;
  int prev_fence_on;    // to record previous state of fence_on variable
  int init_PL;  // set when fence_on switches from 0 to 1 to tell processing loop to perform appropriate iniitalizations

  // Geofence specific variables
  float set_points[8];
  double time_fence_init;
};


#endif
