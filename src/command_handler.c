// Handler function to either pass through RX commands to Naza or else
// copy computer (autonomous control) commands through to Naza.
#define EXTERN extern

#include "../include/quadcopter_main.h"

////////////////////////////////////////////////////////////////////////

void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
		      const channels_t *msg, void *userdata)
{	
  // create a copy of the received message
  channels_t new_msg;
  new_msg.utime = msg->utime;
  new_msg.num_channels = msg->num_channels;
  new_msg.channels = (int16_t*) malloc(msg->num_channels*sizeof(int16_t));
  for(int i = 0; i < msg->num_channels; i++){
    new_msg.channels[i] = msg->channels[i];
  }

  // Copy state to local state struct to minimize mutex lock time
  struct state localstate;
  pthread_mutex_lock(&state_mutex);
  memcpy(&localstate, state, sizeof(struct state));
  pthread_mutex_unlock(&state_mutex);
  
  pilotstate_t stick_msg;
  stick_msg.utime = utime_now();
  stick_msg.thrust_out = new_msg.channels[0];
  stick_msg.roll_out = new_msg.channels[1];
  stick_msg.pitch_out = new_msg.channels[2];
  stick_msg.yaw_out = new_msg.channels[3];
  stick_msg.auto_on = new_msg.channels[7];
  pilotstate_t_publish((lcm_t *) userdata, "PILOT_STATE",&stick_msg);
  
  
  // Check if the autonomous controller should be turned on:
  if(new_msg.channels[7]>1500){
    localstate.fence_on = 1;
    //Check if we just flipped the switch and need to do initialization
    if(localstate.prev_fence_on==0){
      // run initialization code
      fprintf(log_txt," %s: Switch Flip detected: Initializing first waypoint:\n",__func__);
      // set all initialization flags to one
      localstate.init_PL = 1; // processing_loop initialization flag
      
      // set set_points to current position for first loop
      for(int i=0; i < 8; i++){
        localstate.set_points[i] = localstate.pose[i];	      
      }
      fprintf(log_txt,"x: %lf || y: %lf || z: %lf || yaw: %lf\n", localstate.set_points[0], localstate.set_points[1], localstate.set_points[2], localstate.set_points[3]);
    }
  }else{
    localstate.fence_on = 0;	    
  }
  

  // Decide whether or not to edit the motor message prior to sending it
  // set_points[] array is specific to geofencing.  You need to add code 
  // to compute them for our FlightLab application!!!
  printf("State Machine Status: %d\n", state_machine_status);
  float pose[8], set_points[8];
  if(localstate.fence_on == 1){
    if(state_machine_status==SM_PILOT_FLYING){
      state_machine_status = SM_LANDING; // if we are switching from pilot control to autonomous mode
    }
    for(int i = 0; i < 8; i++){
      pose[i] = (float) localstate.pose[i];
      set_points[i] = localstate.set_points[i];
    }
    // hold position at edge of fence
    // This needs to change - mutex held way too long
    if (state_machine_status == SM_LANDING || state_machine_status == SM_TAKEOFF || state_machine_status == SM_UNPERCH){
      auto_control(pose, set_points, new_msg.channels);
    }

  } else{  // Fence off
    // pass user commands through without modifying
    printf("FENCE OFF\n");
    state_machine_status = SM_PILOT_FLYING;
  }


#ifdef MANUAL_PERCH
  new_msg.channels[0] = msg->channels[0];  //<--Thrust Control pass_through
  new_msg.channels[1] = msg->channels[1];  //<--Roll Control pass_through
  new_msg.channels[2] = msg->channels[2];  //<--Pitch Control pass_through
  new_msg.channels[3] = msg->channels[3];  //<--Yaw Control pass_through
#endif




  static long long int start_time;
  static int timer_started = 0;
  if ((state_machine_status == SM_PERCH) && (!timer_started)){
    start_time = utime_now();
    timer_started = 1;
  }
  else if (state_machine_status == SM_PERCH){
    float diff = ((float)(utime_now() - start_time))/1000000;
    if (diff > 0.5 && diff < 1.5) {
      new_msg.channels[0] = new_msg.channels[1] = new_msg.channels[2] = new_msg.channels[3] = 1070;
    }
    if(diff>1.5){
      state_machine_status = SM_PERCHED;
      timer_started = 0;
    }
  }

  if (state_machine_status == SM_UNPERCH && (!timer_started)){
    start_time = utime_now();
    timer_started = 1;
  }
  else if (state_machine_status == SM_UNPERCH){
    float diff = ((float)(utime_now() - start_time))/1000000;
    if (diff > 0 && diff < 1) {
      new_msg.channels[0] = new_msg.channels[1] = new_msg.channels[2] = new_msg.channels[3] = 1070;
    }
    if(diff>1){
      state_machine_status = SM_UNPERCHED;
      timer_started = 0;
    }
  }

  

  
  // new_msg.channels[0] = msg->channels[0];  //<--Thrust Control pass_through
  // new_msg.channels[1] = msg->channels[1];  //<--Roll Control pass_through
  // new_msg.channels[2] = msg->channels[2];  //<--Pitch Control pass_through
  new_msg.channels[3] = msg->channels[3];  //<--Yaw Control pass_through
  
  // set channel 8 of retransmitted message to 1180 to avoid bugs
  new_msg.channels[7] = 1180;
  // send lcm message to motors
  channels_t_publish((lcm_t *) userdata, "CHANNELS_1_TX", &new_msg);

  // Save received (msg) and modified (new_msg) command data to file.
  // NOTE:  Customize as needed (set_points[] is for geofencing)
  fprintf(block_txt,"%ld,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f\n",
	  (long int) msg->utime,msg->channels[0],msg->channels[1],msg->channels[2],
	  msg->channels[3], msg->channels[7],
	  new_msg.channels[0],new_msg.channels[1],new_msg.channels[2], 
	  new_msg.channels[3],new_msg.channels[7],
	  set_points[0],set_points[1],set_points[2],
	  set_points[3],set_points[4],set_points[5],set_points[6],
	  set_points[7]);
  fflush(block_txt);
  
  localstate.prev_fence_on = localstate.fence_on;
  pthread_mutex_lock(&state_mutex);
  memcpy(state, &localstate, sizeof(struct state));
  pthread_mutex_unlock(&state_mutex);
}
