//
// Top-level program for quadcopter
//
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>

#define EXTERN   // main() program definition

#include "../include/quadcopter_main.h"

/*  ########################################
    ###    QUADCOPTER MAIN FUNCTION      ### 
    ########################################

    Code has several threads. 
    Comment the ones you do not need.
    List below show the threads you NEED to run it.

    ### FOR THE QUADROTOR TO FLY ###
    -> lcm_thread_loop
    -> processing_loop
    -> run_motion_capture
    -> (if you use servos) run_dynamixel_comm
    -> (if you use imu)    run_imu

    ### FOR TESTING MOTION CAPTURE ONLY ###
    -> run_motion_capture

    ### FOR TESTING SERVOS ONLY (NOT CONNECTED TO YOUR GRIPPER!) ###
    -> run_dynamixel_comm
    -> set_dynamixel
*/

int main() {

  // Open file to capture command RX, TX, and pertinent guidance data
  block_txt = fopen("block.txt","a");
  log_txt = fopen("log.txt","w");

  state_machine_status = 1; //STATE MACHINE INITIALIZATION!!

  // Initialize the data mutexes
  pthread_mutex_init(&imu_mutex, NULL);
  pthread_mutex_init(&mcap_mutex, NULL);
  pthread_mutex_init(&state_mutex, NULL);
  pthread_mutex_init(&dynamixel_mutex, NULL);

  // Start the threads
  pthread_t lcm_thread;
  pthread_t processing_thread;
  pthread_t imu_thread;
  pthread_t motion_capture_thread;
  pthread_t dynamixel_comm_thread;
  pthread_t dynamixel_set_thread;

  pthread_create(&lcm_thread, NULL, lcm_thread_loop, NULL);
  pthread_create(&processing_thread, NULL, processing_loop, NULL);
  pthread_create(&imu_thread, NULL, run_imu, NULL);
  pthread_create(&motion_capture_thread, NULL, run_motion_capture, NULL);
  pthread_create(&dynamixel_comm_thread, NULL, run_dynamixel_comm, NULL);
  pthread_create(&dynamixel_set_thread, NULL, set_dynamixel, NULL);

  while(1){
    int c;
    scanf("%d", &c);

    switch(c){
      case 1:
        printf("%s Triggering gripper. \n", __func__);
        if (gripper_desired_state == 1)
          gripper_desired_state = 0;
        else if (gripper_desired_state == 0)
          gripper_desired_state = 1;

        printf("%s Gripper State %d \n", __func__, gripper_desired_state);
        break;
      case 2:
        if ( state_machine_status == SM_PERCHED)
        {
          printf("%s triggering unperch sequence\n", __func__);
          state_machine_status = SM_UNPERCH;
        }
        break;
    }
  }


  // Join threads upon completetion
  pthread_join(lcm_thread, NULL);
  pthread_join(processing_thread, NULL);
  pthread_join(imu_thread, NULL);
  pthread_join(motion_capture_thread, NULL);
  pthread_join(dynamixel_comm_thread, NULL);
  pthread_join(dynamixel_set_thread, NULL);


  fclose(log_txt);
  fclose(block_txt);
  return 0;
}

/*
  LCM processing (top-level loop) -- Only use to talk with BLOCKS
*/
void *lcm_thread_loop(void *data){
  printf("%s LCM thread started \n", __func__);
  lcm_t* lcm = lcm_create(NULL);
  channels_t_subscribe(lcm, "CHANNELS_1_RX", channels_handler, lcm);
  while(1)
    lcm_handle(lcm);
  lcm_destroy(lcm);
  return 0;
}

