// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuration parameters from a file (customize please)
//
#define EXTERN extern
#define PROC_FREQ 100 // in Hz

#include "../include/quadcopter_main.h"
void read_config(char *, float *, float *);
int processing_loop_initialize(float *move_alt, float *bar_center);
/*
 * State estimation and guidance thread 
*/
void *processing_loop(void *data){

  int hz = PROC_FREQ;
  static int first_run = 0;
  FILE *fp = NULL;
  // Local copies
  struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
  struct state localstate;
  float bar_center[3] = {0,0,0};
  float start_pos[8] = {0,0,0,0,0,0,0,0};
  float move_alt = 0;
  
  processing_loop_initialize(&move_alt, bar_center);
  
  if(first_run==0){
    first_run = 1;
    fp = fopen("3dvelocities.txt", "w");
  }
  
  lcm_t* lcm = lcm_create(NULL);

  while (1) {  // Main thread loop
  	  
    //Read in state to localstate
    pthread_mutex_lock(&state_mutex);
    memcpy(&localstate, state, sizeof(struct state));
    pthread_mutex_unlock(&state_mutex);  	  

    //Read in global mcap_obs values to local mcobs
    pthread_mutex_lock(&mcap_mutex);
    mcobs[0] = mcap_obs;
    if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
    else               mcobs[1] = mcap_obs;
    pthread_mutex_unlock(&mcap_mutex);
  
    localstate.time = mcobs[0]->time;
    localstate.pose[0] = mcobs[0]->pose[0]; // x position
    localstate.pose[1] = mcobs[0]->pose[1]; // y position
    localstate.pose[2] = mcobs[0]->pose[2]; // z position / altitude
    localstate.pose[3] = mcobs[0]->pose[5]; // yaw angle
    localstate.pose[4] = 0;
    localstate.pose[5] = 0;
    localstate.pose[6] = 0;
    localstate.pose[7] = 0;
    
    // Estimate velocities from first-order differentiation
    double mc_time_step = mcobs[0]->time - mcobs[1]->time;
    if(mc_time_step > 1.0E-7 && mc_time_step < 1){
      localstate.pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
      localstate.pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
      localstate.pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
      localstate.pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;

      //To save velocity estimate
      fprintf(fp,"%lf,%lf,%lf,%lf,%lf\n", ((double)utime_now())/1000000,localstate.pose[4],localstate.pose[5],localstate.pose[6],localstate.pose[7]);
      fflush(fp);   
    }

    //Check for dropout
    //If dropout set reference to current point to stop motion. As no state information available
    if (mcobs[0]->time == mcobs[1]->time){
      printf("%s dropout!!\n", __func__);
      fprintf(log_txt,"%s dropout!!\n", __func__);
    }

  // Geofence activation code
  // turn fence on --> may be useful to control your autonomous controller
 
    machinestate_t machine_msg;
    machine_msg.utime = utime_now();
    machine_msg.machine_state = state_machine_status;
    machinestate_t_publish(lcm, "MACHINESTATE", &machine_msg);
  
    // if we are under autonomous control
    if(localstate.fence_on == 1){
      // update desired state inside "update_set_points"
      update_set_points(localstate.pose, localstate.set_points,localstate.init_PL, bar_center, start_pos, move_alt, lcm); //init_PL indicates first pass on autonomous mode
    }
    localstate_t ls_msg;
    ls_msg.utime = utime_now();
    ls_msg.x = (float)localstate.pose[0];
    ls_msg.y = (float)localstate.pose[1];
    ls_msg.alt = (float)localstate.pose[2];
    ls_msg.yaw = (float)localstate.pose[3];
    ls_msg.x_dot = (float)localstate.pose[4];
    ls_msg.y_dot = (float)localstate.pose[5];
    ls_msg.alt_dot = (float)localstate.pose[6];
    ls_msg.yaw_dot = (float)localstate.pose[7];
    
    localstate_t_publish(lcm, "LOCALSTATE", &ls_msg);
    
    setpnt_t setpnt_msg;
    setpnt_msg.utime = utime_now();
    setpnt_msg.x_set = (float)localstate.set_points[0];
    setpnt_msg.y_set = (float)localstate.set_points[1];
    setpnt_msg.alt_set = (float)localstate.set_points[2];
    setpnt_msg.yaw_set = (float)localstate.set_points[3];
    
    setpnt_t_publish(lcm, "SET_POINT", &setpnt_msg);
    
    
    // If we just finished the initialization of process_loop, set init_PL to 0
    if(localstate.init_PL){
      localstate.init_PL = 0;
    }
    // Copy to global state (minimize total time state is locked by the mutex)
    pthread_mutex_lock(&state_mutex);
    memcpy(state, &localstate, sizeof(struct state));
    pthread_mutex_unlock(&state_mutex);

    usleep(1000000/hz);
  
  } // end while processing_loop()

  return 0;
}

/**
 * update_set_points()
 */
int update_set_points(double* pose, float *set_points, int first_time, float* bar_center, float* start_pos, float move_alt,  lcm_t* lcm){
  //Add your code here to generate proper reference state for the controller
  //generate reference set points for the controller and add to state pointer
  int i;
  static float perch_pos[8];
  // Make sure you set the correct number of total waypoints, otherwise the waypoints will continue to increment
  // #ifndef TEST_SEQUENCE
  //   const int num_wypts = 3;
  // #elif TEST_SEQUENCE
  //   const int num_wypts = 6;
  // #endif

  #ifdef MANUAL_PERCH
    const int num_wypts = 1;
  #endif
  
  float wypt_tol = 0.1; // must be within 10cm of waypoint to move to next
  
  #ifndef TEST_SEQUENCE
    float yaw_tol = (3.1415/180.0)*10;  //yaw tolerance of 90 degrees
  #elif TEST_SEQUENCE
    float yaw_tol = (3.1415/180.0)*90;  //yaw tolerance of 5 degrees
  #endif
  
  float grip_err = 0;
  int grip_status = 0;
  float norm = 0;

  static int wypt_num;
  
  static double holdZ;
  
  norm = sqrt(pow(set_points[0]-pose[0],2)+pow(set_points[1]-pose[1],2)+pow(set_points[2]-pose[2],2));
  
  //Make default behavior to maintain current position
  if(first_time){
    wypt_num = 0;
    for(i=0;i<8;i++){
      start_pos[i] = (float) pose[i];	    
    }
    fprintf(log_txt,"%s: Start Pose set: x:%lf y:%lf z:%lf yaw:%lf\n",__func__,start_pos[0],start_pos[1],start_pos[2],start_pos[3]);
  }

  //Calculate gripper region error
  grip_err = fabs(pose[1]-set_points[1]) + 0.12*tan(fabs(pose[3]));
  grip_status = (fabs(grip_err)<0.03);  
  
  #ifdef WYPT_FOLLOW //If we just want to hold our current position
    if((norm<wypt_tol)&&(fabs(pose[3])<yaw_tol)&&(wypt_num<num_wypts)){ 
      // This will always be called the first time since the initial set_points are set to the start_pos above
      // So, the first waypoint encountered for the purposes of the code below is #1
      wypt_num++;	    
    }

      
   
    //State machine code goes here
    switch(state_machine_status){
      case SM_PILOT_FLYING:
          break;

      case SM_LANDING:
      	  #ifndef TEST_SEQUENCE  //ifNdef!  This is compiled if we're actually trying to perch    
            if(wypt_num==1){      
              set_points[0] = start_pos[0];
              set_points[1] = start_pos[1];
              set_points[2] = -move_alt;  
              set_points[3] = 0;
              holdZ = pose[2];
            }
            if(wypt_num==2){
              set_points[0] = bar_center[0];
              set_points[1] = bar_center[1];
              set_points[2] = -move_alt;
              set_points[3] = 0;
              holdZ = pose[2];
            }
            if(wypt_num==3){
              set_points[0] = bar_center[0];
              set_points[1] = bar_center[1];
              set_points[2] = bar_center[2];
              set_points[3] = 0;
              if(grip_status){
                holdZ = pose[2]; // last altitude at which we were lined up with capture region
                if(fabs(pose[2]-bar_center[2])<0.05){
                  state_machine_status = SM_PERCH;
                }
              } else{
                set_points[2] = holdZ; // to stop descent if we get misaligned   
              }
            }
          #elif TEST_SEQUENCE  //If we're running the test sequence
            if(wypt_num==1){      
              set_points[0] = start_pos[0];
              set_points[1] = start_pos[1]+0.5;
              set_points[2] = start_pos[2];  
              set_points[3] = 0;
            }            
            if(wypt_num==2){      
              set_points[0] = start_pos[0]+0.5; 
              set_points[1] = start_pos[1]+0.5;
              set_points[2] = start_pos[2];  
              set_points[3] = 0;
            }  
            if(wypt_num==3){      
              set_points[0] = start_pos[0]+0.5;
              set_points[1] = start_pos[1];
              set_points[2] = start_pos[2];  
              set_points[3] = 0;
            } 
            if(wypt_num==4){      
              set_points[0] = start_pos[0];
              set_points[1] = start_pos[1];
              set_points[2] = start_pos[2];  
              set_points[3] = 0;
            }    
            if(wypt_num==5){      
              set_points[0] = start_pos[0];
              set_points[1] = start_pos[1];
              set_points[2] = start_pos[2]-0.5;  
              set_points[3] = 0;
            }
            if(wypt_num==6){      
              set_points[0] = start_pos[0];
              set_points[1] = start_pos[1];
              set_points[2] = start_pos[2];  
              set_points[3] = 0;
            }                
          #endif
          #ifdef MANUAL_PERCH
            if(wypt_num==1){
              set_points[0] = bar_center[0];
              set_points[1] = bar_center[1];
              set_points[2] = bar_center[2];
              set_points[3] = 0;
              if(grip_status){
                if(fabs(pose[2]-bar_center[2])<0.04){
                  state_machine_status = SM_PERCH;
                }
              }
            }
          #endif

          break;
      case SM_PERCH: //Trigger gripper closing && turn off motors
          gripper_desired_state = CLOSE_GRIPPER;
          wypt_num = 0;
          break;

      case SM_UNPERCH:
          set_points[0] = pose[0];
          set_points[1] = pose[1];
          set_points[2] = pose[2];  
          set_points[3] = pose[3];
          for(i=0;i<8;i++){
            perch_pos[i] = pose[i];     
          }
          break;

      case SM_UNPERCHED: //Trigger gripper closing && turn off motors
          gripper_desired_state = OPEN_GRIPPER;
          #ifdef GRIPPER_UNPERCH_SEQUENCE
          if(wypt_num==1){      
            set_points[0] = perch_pos[0];
            set_points[1] = perch_pos[1];
            set_points[2] = perch_pos[2]-0.75;  
            set_points[3] = 0;
            holdZ = pose[2];
          }
          if(wypt_num==2){
            set_points[0] = perch_pos[0];
            set_points[1] = perch_pos[1];
            set_points[2] = -move_alt;
            set_points[3] = 0;
            holdZ = pose[2];
          }
          if(wypt_num==3){
            set_points[0] = start_pos[0];
            set_points[1] = start_pos[1];
            set_points[2] = -move_alt;
            set_points[3] = 0;
          }
          #endif
          break;
      
      case SM_HOLD:
          set_points[0] = start_pos[0];
          set_points[1] = start_pos[1];
          set_points[2] = start_pos[2];  
          set_points[3] = 0;
          break;
    }    
  #else
    for(i=0;i<8;i++){
      set_points[i] = start_pos[i];	    
    }  
    set_points[3] = 0;  
  #endif
  // Publish an LCM message with data about waypoint tracking
  wyptdata_t wypt_lcm;
  wypt_lcm.utime = utime_now();
  wypt_lcm.wypt_num = wypt_num;
  wypt_lcm.norm = norm;
  wypt_lcm.yaw_err = (float)pose[3];
  wypt_lcm.y_err = (float)fabs(pose[1]-set_points[1]);
  wypt_lcm.landing_tol = grip_err;
  wypt_lcm.status = grip_status; 
  wyptdata_t_publish(lcm,"WYPT_DATA",&wypt_lcm);

  return 0;
}


/**
 * processing_loop_initialize()
*/
int processing_loop_initialize(float *move_alt, float *bar_center)
{
  // Initialize state struct
  state = (state_t*) calloc(1,sizeof(*state));
  state->time = ((double)utime_now())/1000000;
  memset(state->pose,0,sizeof(state->pose));
  memset(state->set_points,0,sizeof(state->set_points));
  state->fence_on = 0;
  state->prev_fence_on = 0;
  state->init_PL = 0;
  state->time_fence_init = 0;
  
  // Read configuration file to load end points
  char conf_fname[] = {"config.txt"};
  read_config(conf_fname, bar_center, move_alt);

  mcap_obs[0].time = state->time;
  mcap_obs[1].time = -1.0;  // Signal that this isn't set yet

  // Initialize Altitude Velocity Data Structures
  memset(diff_z, 0, sizeof(diff_z));
  memset(diff_z_med, 0, sizeof(diff_z_med));

  return 0;
}

void read_config(char* config, float* bar_center, float* move_alt_ptr){
  fprintf(log_txt,"read_config called:\n");
  // open configuration file
  FILE* conf = fopen(config,"r");
  // holder string
  char str[1000];

  // read in the quadrotor initial position
  //fscanf(conf,"%s %lf %lf %lf",str,landZ[0],landZ[1],landZ[2]);
  fgets(str,1000,conf);
  sscanf(str, "%*s %f %f %f",&bar_center[0],&bar_center[1],&bar_center[2]); 
  fprintf(log_txt, "Bar Center pt: %f, %f, %f\n",bar_center[0],bar_center[1],bar_center[2]);
  fgets(str,1000,conf);
  sscanf(str, "%*s %f",move_alt_ptr);  
  fprintf(log_txt, "Move_Alt: %f\n",*move_alt_ptr);
  fclose(conf);
}
