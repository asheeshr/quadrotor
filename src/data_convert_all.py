#!/usr/bin/python

from os import spawnl, P_NOWAIT, kill
from os import listdir
from os.path import isfile, join
from subprocess import call
import copy
import signal
import glob
import shutil

for i in listdir("/home/student/team11/lcm_data"):
	for j in listdir("/home/student/team11/lcm_data/" + i):
		logs = copy.deepcopy(listdir("/home/student/team11/lcm_data/" + i + "/" + j))
		for k in logs:
			print k
			f = "/home/student/team11/lcm_data/" + i + "/" + j + '/' + k
			print "Selecting ", f
			pid = spawnl(P_NOWAIT,"/home/student/team11/quadcopter/src/data_converter.py")
			print "Spawned converter", pid
			print "Starting lcm-logplayer"
			call(["lcm-logplayer", "--speed=4" , f])
			print "lcm-logplayer finished"
			kill(int(pid), signal.SIGINT)
			print "killed ", pid

			dest_dir = "/home/student/team11/lcm_data/" + i + "/" + j
			for f2 in listdir("/home/student/team11/quadrotor/include/lcmtypes/"):
				if 'csv' in f2:
					print f2
					f3 = "/home/student/team11/quadrotor/include/lcmtypes/" + f2
					shutil.copy(f3, dest_dir)



			# for l in "/home/student/team11/lcm_data/" + i + "/" + j:
			# 	dest_dir = 'cp /home/student/team11/quadrotor/include/lcmtypes/*.csv /home/student/team11/lcm_data/' + i
			# 	call(temp, shell=True)
			# 	print "copied csv to", "/home/student/team11/lcm_data/" + i + "/" + j

