//
// auto_control:  Function to generate autonomous control PWM outputs.
// 
#define EXTERN extern
#include "../include/quadcopter_main.h"

// Define outer loop controller 
// PWM signal limits and neutral (baseline) settings

// THRUST
#define thrust_PWM_up 1575 // Upper saturation PWM limit.
#define thrust_PWM_base 1500 // Zero z_vela PWM base value. 
#define thrust_PWM_down 1425 // Lower saturation PWM limit. 
  
// ROLL
#define roll_PWM_left 1620  // Left saturation PWM limit.
#define roll_PWM_base 1500  // Zero roll_dot PWM base value. 
#define roll_PWM_right 1380 //Right saturation PWM limit. 

// PITCH
#define pitch_PWM_forward 1620  // Forward direction saturation PWM limit.
#define pitch_PWM_base 1500 // Zero pitch_dot PWM base value. 
#define pitch_PWM_backward 1380 // Backward direction saturation PWM limit. 

// YAW
#define yaw_PWM_ccw 1575 // Counter-Clockwise saturation PWM limit (ccw = yaw left).
#define yaw_PWM_base 1500 // Zero yaw_dot PWM base value. 
#define yaw_PWM_cw 1425 // Clockwise saturation PWM limit (cw = yaw right). 

int pid_loop(float input[4], float setpoint[], float setpoint_dot[], float *output);

lcm_t* lcm = lcm_create(NULL);

// Outer loop controller to generate PWM signals for the Naza-M autopilot
void auto_control(float *pose, float *set_points, int16_t* channels_ptr)
{ 
  // pose (size 8):  actual {x, y , alt, yaw, xdot, ydot, altdot, yawdot}
  // set_points (size 8):  reference state (you need to set this!) 
  //                       {x, y, alt, yaw, xdot, ydot, altdot, yawdot} 

  // channels_ptr (8-element array of PWM commands to generate in this function)
  // Channels for you to set:
  // [0] = thrust
  // [1] = roll
  // [2] = pitch
  // [3] = yaw
  //float set_points_processed[8];
  float input[4] = {pose[2], pose[1], pose[0], pose[3]}; // Need to be scaled??
  float setpoint[4] = {set_points[2], set_points[1], set_points[0], set_points[3]}; //Check units and scale
  float setpoint_dot[4] = {0, 0, 0, 0}; //Check units and scale
  float output[4] = {0, 0, 0, 0};

  //generate thrust,y,p,r from set_points and pose
  pid_loop(input, setpoint, setpoint_dot, output);
  
  pidstate_t pidstate;
  pidstate.utime = utime_now();
  pidstate.thrust_out = output[0];
  pidstate.roll_out = output[1];
  pidstate.pitch_out = output[2];
  pidstate.yaw_out = output[3];
  
  pidstate_t_publish(lcm,"PID_OUTPUTS",&pidstate);
  
  
  for(int i=0; i<4; i++){
    channels_ptr[i] = output[i];
  }
  
  return;
}

float pid_update(float input, float setpoint, float setpoint_dot, float kp, float ki, float kd, float i_term_min, float i_term_max, float output_min, float output_max, float trim, 
  float *i_term, float *setpoint_prev, float *error_prev, float *input_prev)
{
  //static setpoint_prev, input_prev, error_prev;
  //float setpoint_dot = setpoint - *setpoint_prev;  
  float error = setpoint - input;
  //error = ScaleNegativeFloat(error, -100, 100);
  float error_dot = error - *error_prev;
  (*error_prev) = error;
  
  float p_term = kp*error;
  (*i_term) += ki*error;
  float d_term = kd*error_dot;
  
  if((*i_term) < i_term_min) (*i_term) = i_term_min;
  if((*i_term) > i_term_max) (*i_term) = i_term_max;
  
  if(setpoint_dot < 0){
    trim = -trim; //trim will contribute towards direction of motion
  }
  if(setpoint_dot > 0){
    trim = trim; //therefore trim must be passed as magnitude!
  }

  float output = p_term + (*i_term) + d_term + trim;

  if(output < output_min){
    output = output_min;
  }
  if(output > output_max){
    output = output_max;
  }

  *input_prev = input;
  *setpoint_prev = setpoint;

  return output;
}

void pid_init(float kp[], float ki[], float kd[], float i_term_min[], float i_term_max[], float output_min[], float output_max[], float trim[])
{
  //Read in data from config.txt
  FILE* conf = fopen("config_pid.txt","r");

  fscanf(conf,"kp %f %f %f %f\n",&kp[0],&kp[1],&kp[2],&kp[3]);
  fscanf(conf,"ki %f %f %f %f\n",&ki[0],&ki[1],&ki[2],&ki[3]);
  printf("ki %f %f %f %f\n",ki[0],ki[1],ki[2],ki[3]);
  fscanf(conf,"kd %f %f %f %f\n",&kd[0],&kd[1],&kd[2],&kd[3]);
  printf("kd %f %f %f %f\n",kd[0],kd[1],kd[2],kd[3]);
  fscanf(conf,"i_term_min %f %f %f %f\n",&i_term_min[0],&i_term_min[1],&i_term_min[2],&i_term_min[3]);
  fscanf(conf,"i_term_max %f %f %f %f\n",&i_term_max[0],&i_term_max[1],&i_term_max[2],&i_term_max[3]);  
  fscanf(conf,"output_min %f %f %f %f\n",&output_min[0],&output_min[1],&output_min[2],&output_min[3]);
  printf("output_min %f %f %f %f\n",output_min[0],output_min[1],output_min[2],output_min[3]);
  fscanf(conf,"output_max %f %f %f %f\n",&output_max[0],&output_max[1],&output_max[2],&output_max[3]);
  printf("output_max %f %f %f %f\n",output_max[0],output_max[1],output_max[2],output_max[3]);
  fscanf(conf,"trim %f %f %f %f\n",&trim[0],&trim[1],&trim[2],&trim[3]);
  printf("trim %f %f %f %f\n",trim[0],trim[1],trim[2],trim[3]);

  fclose(conf);

}


int pid_loop(float input[4], float setpoint[], float setpoint_dot[], float *output)
{
  static int first_run = 0;
  static float kp[4], ki[4], kd[4], i_term_min[4], i_term_max[4], output_min[4], output_max[4], trim[4]; // Data to be stored in config file
  static float i_term[4], setpoint_prev[4], error_prev[4], input_prev[4];

  if(first_run==0){
    pid_init(kp, ki, kd, i_term_min, i_term_max, output_min, output_max, trim);
    first_run = 1;
  }

  for(int i=0; i<4; i++){
    output[i] = pid_update(input[i], setpoint[i], setpoint_dot[i], kp[i], ki[i], kd[i], i_term_min[i], i_term_max[i], output_min[i], output_max[i], trim[i],  
                            &i_term[i], &setpoint_prev[i], &error_prev[i], &input_prev[i]);
  }

  return 0;
}
