#define EXTERN extern
#include "../include/quadcopter_main.h"
//#define SERVO_TEST 1

void open_gripper()
{
    pthread_mutex_lock(&dynamixel_mutex);
    printf("Open: Angle Mode\n");

    bus.servo[0].cmd_angle = 230.0; 
    bus.servo[0].cmd_speed = 0.5; 
    bus.servo[0].cmd_flag = CMD;
    bus.servo[0].cmd_torque = 0.6;
    bus.servo[1].cmd_angle = 230.0; 
    bus.servo[1].cmd_speed = 0.5; 
    bus.servo[1].cmd_flag = CMD;
    bus.servo[1].cmd_torque = 0.6;
    pthread_mutex_unlock(&dynamixel_mutex);
    
    /*
    pthread_mutex_lock(&dynamixel_mutex);
    Dynam_PrintStatus(&(bus.servo[0]));
    Dynam_PrintStatus(&(bus.servo[1]));
    pthread_mutex_unlock(&dynamixel_mutex);
    */
}

void close_gripper()
{
    pthread_mutex_lock(&dynamixel_mutex);
    printf("Close: Angle Mode\n");

    bus.servo[0].cmd_angle = 40.0;
    bus.servo[0].cmd_speed = 0.5;
    bus.servo[0].cmd_flag = CMD;
    bus.servo[0].cmd_torque = 1;
    bus.servo[1].cmd_angle = 40.0;
    bus.servo[1].cmd_speed = 0.5;
    bus.servo[1].cmd_flag = CMD;
    bus.servo[1].cmd_torque = 1;
    pthread_mutex_unlock(&dynamixel_mutex);
    /*
    pthread_mutex_lock(&dynamixel_mutex);
    Dynam_PrintStatus(&(bus.servo[0]));
    Dynam_PrintStatus(&(bus.servo[1]));
    pthread_mutex_unlock(&dynamixel_mutex);
    */
}

void init_servos()
{
    open_gripper();
    sleep(1);
    close_gripper();
    sleep(1);
    open_gripper();
    sleep(1);
    gripper_desired_state = GRIP_STATUS_OPEN;
}


void * set_dynamixel(void * var){ 


#ifdef SERVO_TEST
    int i = 0, c = -1;
    for(;i < 100;i++){
        printf("%s input servo command \n", __func__);
        printf("Servo command before scanf: %d\n", c);
        scanf("%d", &c);        
        switch(c % 7){
            case 0:
                printf("Open: Angle Mode\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_angle = 230.0; 
                bus.servo[0].cmd_speed = 0.1; 
                bus.servo[0].cmd_flag = CMD;
                bus.servo[1].cmd_angle = 230.0; 
                bus.servo[1].cmd_speed = 0.1; 
                bus.servo[1].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 1:
                printf("Close: Angle Mode\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_angle = 40.0;
                bus.servo[0].cmd_speed = 0.1;
                bus.servo[0].cmd_flag = CMD;
                bus.servo[1].cmd_angle = 40.0;
                bus.servo[1].cmd_speed = 0.1;
                bus.servo[1].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 2:
                printf("Change to Wheel Mode - Stop the Servo\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_mode = WHEEL; 
                bus.servo[0].cmd_flag = MODE;
                bus.servo[1].cmd_mode = WHEEL; 
                bus.servo[1].cmd_flag = MODE;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 3:     
                printf("Command 0.3 speed\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_speed = 0.7; 
                bus.servo[0].cmd_flag = CMD;
                bus.servo[1].cmd_speed = 0.7; 
                bus.servo[1].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 4:
                printf("Request Status:\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_flag = STATUS;
                bus.servo[1].cmd_flag = STATUS;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 5:     
                printf("Command -0.3 speed\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_speed = -0.7; 
                bus.servo[0].cmd_flag = CMD;
                bus.servo[1].cmd_speed = -0.7; 
                bus.servo[1].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 6:
                printf("Change to Joint Mode - Go to default position\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_mode = JOINT; 
                bus.servo[0].cmd_flag = MODE;
                bus.servo[1].cmd_mode = JOINT; 
                bus.servo[1].cmd_flag = MODE;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
        }
        fflush(stdout);
        sleep(0.5);

        /* If we requested Status Print Status */
        pthread_mutex_lock(&dynamixel_mutex);
        if(i%7 == 4) Dynam_PrintStatus(&(bus.servo[0]));
        if(i%7 == 4) Dynam_PrintStatus(&(bus.servo[1]));
        pthread_mutex_unlock(&dynamixel_mutex);
    }

#else

    init_servos();


    while(1){
        static int gripper_position; //0 - OPEN, 1- CLOSED

        switch(gripper_desired_state) //Global Variable- Should be set from processing_loop
        {
            case CLOSE_GRIPPER:
                    if(gripper_position==GRIP_STATUS_CLOSED){
                        usleep(10000);
                    }
                    else if(gripper_position==GRIP_STATUS_OPEN){
                        printf("%s attempting to close gripper\n", __func__);
                    
                        close_gripper();
                        gripper_position = GRIP_STATUS_CLOSED;
                    }
                    break;
            case OPEN_GRIPPER: 
                    if(gripper_position==GRIP_STATUS_CLOSED){
                        printf("%s attempting to open gripper\n", __func__);
                    
                        open_gripper();
                        gripper_position = GRIP_STATUS_OPEN;
                    }
                    else if(gripper_position==GRIP_STATUS_OPEN){
                        usleep(10000);
                    }
                    break;
        }
    }
#endif
}
